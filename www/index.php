<?php

  include 'helper_functions.php';

  $roster = json_decode(file_get_contents("class-roster.json"), true);

  if(is_null($_SESSION['student_order'])){
    $_SESSION['student_order'] = randomize_IDs($roster);
  }
  // use the functions you write in helper_functions.php to write a script that
  //   1. fills in the current student's name based on the student chosen in the query string
  //   2. fills in the next student's id based on the randomized order stored in $_SESSION['student_order']
$current_student_name = retrieve_Student(key($_SESSION['student_order']), $_SESSION['student_order']);

$next_student_id = next_Student($_SESSION['student_order']);
  // follow the instructions in reset.php to re-start the user's session

?><!DOCTYPE>
<html>
  <head>
    <title>Lab 07</title>
  </head>
  <body>
    <a href="reset.php">reset</a>
    <h1>It's time for <?= $current_student_name ?> to answer </h1>
      <a href="/?id=<?= $next_student_id ?>">next student</a>
  </body>
</html>
