<?php 

// 1. make a function called randomize_IDs which:
//    a. store the ids from the passed in list (e.g. student-01) in an array
//    b. randomize the order of the newly created id array
//    c. return the randomized id array 
//      
//  note that this function should not alter the original list
//
function randomize_IDs($list){
    shuffle($list);
    return $list; //=> ["student-03", "student-07", etc...]
}

// -------------------

// 2. make a function called retrieve_Student which:
//     a. accepts as its first argument a particular id to retrieve
//     b. accepts as its second argument a list of items in the format of class-roster.json
//     c. returns a student's name based on the id given
//
function retrieve_Student($student_id, $class_roster_array){
    return $class_roster_array[$student_id]["name"];
}//=> "Lawrence Arden"

// -------------------

// 3. make a function called next_Student which:
//     a. accepts as its first argument the current student
//     b. accepts as its second argument the student order array
//     c. return the id of the next member of the student order array
//         if the current student is the last member of the list,
//         the list should start over
//
function next_Student($student_order){
        $next_student_id = next($student_order);
    return $next_student_id;
}

